# Generated by Django 1.11.3 on 2017-07-18 12:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sso_auth', '0002_auto_20170702_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='u2fdevice',
            name='version',
            field=models.TextField(default='U2F_V2'),
        ),
    ]
