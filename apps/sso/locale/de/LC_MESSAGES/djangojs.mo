��          L      |       �      �      �      �      �   e     B  u     �  !   �     �        o   7                                         Error loading image file. Image is too large (max %skB). Image type is not allowed. No location found The map was updated with the new coordinates. Press the 'Save' button for saving the new coordinates. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Fehler beim Laden des Bilds Das Bild ist zu groß (max %skB). Bildtyp wird nicht unterstützt Es wurde keine Position gefunden Die Koordinaten in der Karte wurden aktualisiert. Zum Speichern der neuen Koordinaten bitte "Sichern" drücken. 