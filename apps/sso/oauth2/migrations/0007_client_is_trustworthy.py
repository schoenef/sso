# Generated by Django 2.0.2 on 2018-02-17 21:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oauth2', '0006_auto_20180112_2142'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='is_trustworthy',
            field=models.BooleanField(default=False, verbose_name='trustworthy'),
        ),
    ]
