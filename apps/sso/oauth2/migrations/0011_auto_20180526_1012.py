# Generated by Django 2.0.5 on 2018-05-26 08:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('oauth2', '0010_auto_20180526_0755'),
    ]

    operations = [
        migrations.RenameField(
            model_name='client',
            old_name='is_using_pkce',
            new_name='force_using_pkce',
        ),
    ]
