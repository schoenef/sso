# Generated by Django 2.2.4 on 2019-08-25 19:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oauth2', '0012_auto_20180526_1012'),
    ]

    operations = [
        migrations.AddField(
            model_name='authorizationcode',
            name='nonce',
            field=models.CharField(blank=True, max_length=2047, verbose_name='Nonce'),
        ),
    ]
