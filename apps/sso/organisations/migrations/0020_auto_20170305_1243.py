# Generated by Django 1.10.6 on 2017-03-05 11:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('l10n', '0002_country_last_modified'),
        ('organisations', '0019_auto_20170303_2110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organisationcountry',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='l10n.Country', verbose_name='country'),
        ),
        migrations.AlterUniqueTogether(
            name='organisationcountry',
            unique_together=set([('country', 'association')]),
        ),
    ]
