# Generated by Django 2.0.2 on 2018-03-01 21:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organisations', '0029_auto_20180114_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organisation',
            name='coordinates_type',
            field=models.CharField(blank=True, choices=[('2', 'City/Village'), ('3', 'Exact'), ('4', 'Nearby')], db_index=True, default='3', max_length=1, verbose_name='coordinates type'),
        ),
    ]
