# Generated by Django 1.10.5 on 2017-02-24 22:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organisations', '0017_auto_20170224_2309'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='adminregion',
            name='country',
        ),
        migrations.RemoveField(
            model_name='organisation',
            name='country',
        ),
    ]
